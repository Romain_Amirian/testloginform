var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
  secret: process.env.JWT_SECRET,
  userProperty: 'payload'
});
var ctrlUser = require('../controllers/user');
var ctrlAuth = require('../controllers/authentication');

//Users authentication
router.post('/users/register', ctrlAuth.registerUser);
router.post('/users/login', ctrlAuth.loginUser);

//Users
router.get('/users', auth, ctrlUser.getUsers);
//router.get('/users', ctrlUser.getUsers);
router.get('/users/:user_id', auth, ctrlUser.getOneUser);
//router.get('/users/:user_id', ctrlUser.getOneUser);

//Credentials operations
router.post('/users/:user_id', auth, ctrlUser.getOneAuthUser); //register
router.put('/users/:user_id', auth, ctrlUser.changeUser); //change password login image
router.delete('/users/:user_id', auth, ctrlUser.removeUser); //remove


module.exports = router;
