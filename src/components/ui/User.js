//User.js
import React from 'react';
import { Link } from 'react-router-dom';

const User = (props) => (
  <div>
    <Link to={`/users/${props._id}`}><img src={props.image.data} alt="user" /><h3>{props.login}</h3></Link>
  </div>
);

User.defaultProps = {
  _id: '',
  image: {data: ''},
  login: ''
};

export default User;
