//Home.js
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import '../../stylesheets/Home.scss';

const Home = ({isLoggedIn, authorizedUser}) =>
    <div className="home-content">
      <div className="home-map">
        <h1>LOGIN FORM WITH MERN STACK</h1>
      </div>
      <div className="home-description">
        <p>Barev dzez guys. Here You're able to register and sign in. And if You're authorized You're able to check all users registered to this server.
        </p>
        {isLoggedIn ? (<div className="greet-user">Hi, <span>{authorizedUser.login}</span></div>) : (
          <div>
            <Link to='/login'>Sign In</Link>
            <Link to='/register'>Sign Up</Link>
          </div>
        )}
        {isLoggedIn ? <Link to='/users'>Go To Users</Link> : <p className="not-authorized">You need to be authorized before got to users list.</p> }
      </div>
    </div>

Home.propTypes = {
    isLoggedIn: PropTypes.bool,
    authorizedUser: PropTypes.object
}

export default Home;
