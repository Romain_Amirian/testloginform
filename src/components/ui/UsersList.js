//UsersList.js
import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import '../../stylesheets/UsersList.scss';

const UsersList = ({isLoggedIn, loadingList, dataUsers, getUsersData=f=>f}) =>
        <div className="users-list">
          { (dataUsers.length) ? (console.log('still empty')) : (getUsersData(dataUsers)) }
          <h2>Users:</h2>
          {(!dataUsers.length) ? (<div>Loading List...</div>) : (dataUsers.map((data) =>
            data._id ? (
              <Link key={data._id} to={'users/' + data._id}>
                <img src={data.image.data ? data.image.data : '/images/default_user.png'} alt="user" /><h3>{data.login}</h3>
              </Link>
            ) : (<p class="status-message">{data}</p>)
          ))}
        </div>

UsersList.propTypes = {
    isLoggedIn: PropTypes.bool,
    loadingList: PropTypes.bool,
    dataUsers: PropTypes.array,
    getUsersData: PropTypes.func
}

export default withRouter(UsersList);
