//UserDetailed.js
import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import '../../stylesheets/UserDetailed.scss';

const UserDetailed = ({match, currentUser, getCurrentUser=f=>f}) =>
        <div className="users-list">
          {getCurrentUser(match.params.user_id)}
          {currentUser ? (
            <div className="user-detailed">
              <div className="user-details">
                <div className="user-fields">
                  <img src={currentUser.image.data ? ('/' + currentUser.image.data) : ('/default_thumb.png')} alt='user' />
                  <article>
                    <p>Login: {currentUser.login}</p>
                  </article>
                </div>
              </div>
            </div>
          ) : (<div>loading...</div>)}
        </div>

UserDetailed.propTypes = {
    match: PropTypes.object,
    currentUser: PropTypes.object,
    getCurrentUser: PropTypes.func
}

export default withRouter(UserDetailed);
