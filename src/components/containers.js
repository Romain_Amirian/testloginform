import { connect } from 'react-redux';
//import { compose } from 'redux';
import Menu from './ui/Menu';
import Home from './ui/Home';
import LoginForm from './ui/LoginForm';
import RegisterForm from './ui/RegisterForm';
import UsersList from './ui/UsersList';
import UserDetailed from './ui/UserDetailed';
import Footer from './ui/Footer';
import UserCredentialsForm from './ui/UserCredentialsForm';
import { getUsers, getOneUser, login, logout, toggleClass, routeLocationDidUpdate, registerUser, changeUser, removeUser, authorizationUser } from '../actions'

export const RegisterUser = connect(
    state =>
        ({
            redirToLogin: state.redirToLogin,
            emptyUser: state.emptyUser
        }),
    dispatch =>
        ({
            registerNewUser(createdUser) {
                dispatch(registerUser(createdUser))
            }
        })
)(RegisterForm)

export const LoginUser = connect(
    state =>
        ({
            redirToUsers: state.redirToUsers,
            status: state.emptyUser.response.status
        }),
    dispatch =>
        ({
            loginUser(userCreds) {
                dispatch(login(userCreds))
            }
        })
)(LoginForm)

export const UserCredsChange = connect(
  state =>
      ({
          _id: state.authorizedUser._id,
          login: state.authorizedUser.login,
          image: state.authorizedUser.image,
          status: state.emptyUser.response.status,
          isLoggedIn: state.isLoggedIn
      }),
  dispatch =>
      ({
          changeCurrentUser(changedUser) {
              dispatch(changeUser(changedUser))
          },
          removeCurrentUser(curuser_id) {
              dispatch(removeUser(curuser_id))
          }
      })
)(UserCredentialsForm)

export const MainMenu = connect(
  state =>
      ({
          isLoggedIn: state.isLoggedIn,
          authorizedUser: state.authorizedUser,
          currentUser: state.signIn.currentUser,
          redirToHome: state.redirToHome,
          toggle: state.toggle
      }),
  dispatch =>
      ({
          logoutUser() {
              dispatch(logout())
          },
          toggleProfile(cur) {
              dispatch(toggleClass(cur))
          }
      })
)(Menu)

export const FooterAuth = connect(
  state =>
      ({
          authorizedUser: state.authorizedUser,
          signedUser: state.signIn.currentUser
      }),
  dispatch =>
      ({
          authorizeUser(auth) {
              dispatch(authorizationUser(auth))
          },
          locationUpdate(loc) {
            dispatch(routeLocationDidUpdate(loc))
          }
      })
)(Footer)

export const Users = connect(
  state =>
      ({
          isLoggedIn: state.isLoggedIn,
          dataUsers: state.dataUsers
      }),
  dispatch =>
      ({
          getUsersData(usrLst) {
              dispatch(getUsers(usrLst))
          }
      })
)(UsersList)

const findById = (datUsr, userId) => {
  var filtered = datUsr.filter(item => item._id === userId);
  filtered = filtered[0];
  return filtered;
};

//state.match.params.user_id
export const OneUser = connect(
  state => ({
      currentUser: findById(state.dataUsers, state.currentUser)
  }),
  dispatch =>
    ({
        getCurrentUser(id) {
            dispatch(getOneUser(id))
        }
    })
)(UserDetailed)


export const HomePage = connect(
  state =>
      ({
          isLoggedIn: state.isLoggedIn,
          authorizedUser: state.authorizedUser
      })
)(Home)

/*
export const Colors = connect(
    ({colors}, {match}) =>
        ({
            colors: sortColors(colors, match.params.sort)
        }),
    dispatch =>
        ({
            onRemove(id) {
                dispatch(removeColor(id))
            },
            onRate(id, rating) {
                dispatch(rateColor(id, rating))
            }
        })
)(ColorList)
*/
