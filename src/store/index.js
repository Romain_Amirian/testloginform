import { createStore, combineReducers, applyMiddleware } from 'redux'
import reduxThunk from 'redux-thunk'
import { emptyUser, isLoggedIn, loadingList, redirToHome, redirToLogin, toggle, redirToUsers, redirBackToUsers, authorizedUser, signIn, dataUsers, currentUser } from './reducers'
import stateData from '../data/initialState'

/*
var initialState = {
  "userForChange": { "_id": "", "login": "", "email": "", "password": "", "image": "", "passwordsec": "", "distinctPass": ""},
  "sure": false,
  "emptyUser": { "login": "", "email": "", "password": "", "image": "", "passwordsec": "", "distinctPass": false },
};
*/

const logger = store => next => action => {
    let result
    console.groupCollapsed("dispatching", action.type)
    console.log('prev state', store.getState())
    console.log('action', action)
    result = next(action)
    console.log('next state', store.getState())
    console.groupEnd()
    return result
}

const saver = store => next => action => {
    let result = next(action)
    localStorage['login-testtask'] = JSON.stringify(store.getState())
    return result
}

const storeFactory = (initialState=stateData) =>
    applyMiddleware(logger, saver, reduxThunk)(createStore)(
        combineReducers({ emptyUser, isLoggedIn, toggle, loadingList, redirToHome, redirToLogin, redirToUsers, redirBackToUsers, authorizedUser, signIn, dataUsers, currentUser}),
        (localStorage['login-testtask']) ?
            JSON.parse(localStorage['login-testtask']) :
            stateData
    )

export default storeFactory
