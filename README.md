"npm install";

Local development:

1. Make sure you have .env file with thisIsSecret peroperty and check your NODE_ENV for development mode use development
2. Make sure mongodb is running
3. "npm run-script start-back-dev" for development server start
4. "npm run-script start-dev" for development client start

Deployment:

1. "npm run-script build" to build react front end part
2. "npm start" to run built front end part
3. Use "localhost:3001" for client
